FROM openjdk:8-jdk-alpine

RUN apk update

ADD startup.sh /tmp/startup.sh
RUN chmod +x /tmp/startup.sh

VOLUME /tmp
ARG JAR_FILE
ADD ${JAR_FILE} demo.jar

ENTRYPOINT ["/tmp/startup.sh"]